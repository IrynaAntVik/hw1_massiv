public class Hw1massiv1 {
    int [] arr = new int [8];
    int a = -20;
    int b = 40;

    public void massivvivod() {
        System.out.println("Элементы массива");
        for (int i = 0; i < arr.length; i++) {
            arr [i] = a + (int) (Math.random()*b);
            System.out.print(arr [i] + "  ");
        }
    }

    public void massivmin() {
        System.out.println("");
        int y = arr [0];
        int n = 1;
        for (int i = 0; i < arr.length - 1; i++) {
            if (y > arr [i+1]){
                y = arr [i+1];
                n = i + 2;
            } else {
                continue;
            }
        }
        System.out.println(y + " - минимальный элемент массива");
        System.out.println(n + " - индекс минимального элемента массива");
    }

    public void massivmax() {
        int y = arr [0];
        for (int i = 0; i < arr.length - 1; i++) {
            if (y > arr [i+1]){
                continue;
            } else {
                y = arr [i+1];
            }
        }
        System.out.println(y + " - максимальный элемент массива");
    }
}
